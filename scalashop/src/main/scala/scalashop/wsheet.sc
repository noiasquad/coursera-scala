package scalashop

object wsheet {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet

  val w = 1                                       //> w  : Int = 1
  val h = 3                                       //> h  : Int = 3
  val src = new Img(w, h)                         //> src  : scalashop.Img = scalashop.package$Img@35f983a6
  val dst = new Img(w, h)                         //> dst  : scalashop.Img = scalashop.package$Img@7f690630
  src(0, 0) = 0;
  src(0, 1) = 3;
  src(0, 2) = 6;

  val stripWidth = src.width / clamp(32, 2, src.width)
                                                  //> stripWidth  : Int = 1
  val xStarts = if (src.width > 1) 0 until src.width by stripWidth else 0 until 1
                                                  //> xStarts  : scala.collection.immutable.Range = Range(0)
  val xEnds = xStarts.map { start => math.min(start + stripWidth, src.width) }
                                                  //> xEnds  : scala.collection.immutable.IndexedSeq[Int] = Vector(1)

  val tasks = (xStarts zip xEnds)                 //> tasks  : scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((0,1))

  def f(u: Double, v: Double): Double =
    (u + v) / (1.0 + u * v)                       //> f: (u: Double, v: Double)Double
  def err(lst: List[Double]): Double =
    lst.reduceLeft(f) - lst.reduceRight(f)        //> err: (lst: List[Double])Double
  def testAssoc: Double = {
    val r = new scala.util.Random
    val lst = List.fill(400)(r.nextDouble * 0.002)
    err(lst)
  }                                               //> testAssoc: => Double
  
  testAssoc                                       //> res0: Double = -7.216449660063518E-16
  testAssoc                                       //> res1: Double = -4.440892098500626E-16
  testAssoc                                       //> res2: Double = -3.3306690738754696E-16
  testAssoc                                       //> res3: Double = 0.0
  testAssoc                                       //> res4: Double = 3.3306690738754696E-16
  testAssoc                                       //> res5: Double = -5.551115123125783E-16
}