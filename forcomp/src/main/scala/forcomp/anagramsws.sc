package forcomp

object anagramsws {
  import Anagrams._

  wordOccurrences("abb")                          //> res0: forcomp.Anagrams.Occurrences = List((a,1), (b,2))

  combinations(wordOccurrences("aabb"))           //> res1: List[forcomp.Anagrams.Occurrences] = List(List(), List((b,1)), List((b
                                                  //| ,2)), List((a,1)), List((a,1), (b,1)), List((a,1), (b,2)), List((a,2)), List
                                                  //| ((a,2), (b,1)), List((a,2), (b,2)))
                                                  
 combinations(wordOccurrences("abab"))            //> res2: List[forcomp.Anagrams.Occurrences] = List(List(), List((b,1)), List((b
                                                  //| ,2)), List((a,1)), List((a,1), (b,1)), List((a,1), (b,2)), List((a,2)), List
                                                  //| ((a,2), (b,1)), List((a,2), (b,2)))

  subtract(wordOccurrences("abbcc"), wordOccurrences("c"))
                                                  //> res3: forcomp.Anagrams.Occurrences = List((a,1), (b,2), (c,1))
  
  sentenceAnagrams(List("I","love","you"))        //> res4: List[forcomp.Anagrams.Sentence] = List(List(you, I, love), List(you, I
                                                  //| o, Lev), List(you, Lev, Io), List(you, love, I), List(you, olive), List(I, y
                                                  //| ou, love), List(I, love, you), List(Io, you, Lev), List(Io, Lev, you), List(
                                                  //| Lev, you, Io), List(Lev, Io, you), List(love, you, I), List(love, I, you), L
                                                  //| ist(olive, you))

  sentenceAnagrams(List("Yes","man"))             //> res5: List[forcomp.Anagrams.Sentence] = List(List(my, en, as), List(my, as, 
                                                  //| en), List(my, sane), List(my, Sean), List(yes, man), List(en, my, as), List(
                                                  //| en, as, my), List(men, say), List(as, my, en), List(as, en, my), List(say, m
                                                  //| en), List(man, yes), List(sane, my), List(Sean, my))

	def sentenceAnagramsMemo(sentence: Sentence): List[Sentence] = {
    def anagrams(occ: Occurrences, cache: Map[Occurrences,List[Sentence]]): Map[Occurrences,List[Sentence]] = {
    	println ("cache:" + cache);
      if (occ.isEmpty) cache.updated(occ, List(Nil))
      else if (cache.contains(occ)) { println ("CACHED"); cache}
      else {
        cache.updated(occ, (for {
          subOcc <- combinations(occ)
          word <- dictionaryByOccurrences(subOcc)
          reducedOcc = subtract(occ, subOcc)
          sentence <- anagrams(reducedOcc, cache)(reducedOcc)
        } yield word :: sentence))
      }
    }
    val sentenceOcc = sentenceOccurrences(sentence)
    anagrams(sentenceOcc, Map())(sentenceOcc)
  }                                               //> sentenceAnagramsMemo: (sentence: forcomp.Anagrams.Sentence)List[forcomp.Ana
                                                  //| grams.Sentence]
	
	sentenceAnagramsMemo(List("youyou"))      //> cache:Map()
                                                  //| cache:Map()
                                                  //| cache:Map()
                                                  //| res6: List[forcomp.Anagrams.Sentence] = List(List(you, you))

}