package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

// Minimum of a heap with one element is the element
  property("minimum-of-heap-having-1-element") = forAll { a: A =>
    val h = insert(a, empty)
    findMin(h) == a
  }

// If you insert any two elements into an empty heap, finding the minimum of the resulting heap should get the smallest of the two elements back.
  property("minimum-of-heap-having-2-elements") = forAll { (a: A, b: A) =>
    val h = insert(b, insert(a, empty))
    val min = findMin(h)
    if (a <= b) min == a else min == b
  }
  
// Minimum element of any heap re-added to it stays the minimum of the resulting heap
  property("minimum-of-any-heap-stays-minimum-after-readd") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }
  
// If you insert an element into an empty heap then delete the minimum, the resulting heap should be empty.
  property("singleton-empty-after-deletion-of-minimum") = forAll {a: A =>
    val h = insert(a, empty)
    isEmpty(deleteMin(h))
  }

// If you insert an element into an empty heap then delete the minimum, the resulting heap should be empty.
  property("same-after-delete-insert-findMin") = forAll { a: A =>
    val h = insert(a, empty)
    a == findMin(insert(a, deleteMin(h)))
  }

// If you insert an element into an empty heap then delete the minimum, the resulting heap should be empty.
  property("one-element-less-after-deletion") = forAll { (h: H, a: A) =>
    val h = insert(a, insert(a, insert(a, empty)))
    isEmpty(deleteMin(deleteMin(deleteMin(h))))
  }

// Given any heap, you should get a sorted sequence of elements when continually finding and deleting minima. (Hint: recursion and helper functions are your friends.)
  property("sorted-sequence-of-minimums-during-deletions") = forAll { (h: H) =>
    if (isEmpty(h)) true
    else check(deleteMin(h), findMin(h))
  }
  
  def check(h1: H, lastMin: A): Boolean = {
    if (isEmpty(h1)) true
    else {  
      val m = findMin(h1)
      lastMin <= m && check(deleteMin(h1), m)
    }
  }

// Finding a minimum of the melding of any two heaps should return a minimum of one or the other.
  property("minimum-of-melded-2-heaps") = forAll { (h1: H, h2:H) =>
    if (isEmpty(h1) || isEmpty(h2)) true
    else {
      val m1 = findMin(h1)
      val m2 = findMin(h2)
      List(m1,m2) contains findMin(meld(h1,h2))
    }
  }

// Given two singletons a and b, a<b, deleteMin after fusion gives b on finding minimum
  property("minimum-of-melded-2-singletons-after-deletion-of-min") = forAll { (a: A, b: A) =>
    if (a < b) {
      val h1 = insert(a, empty)
      val h2 = insert(b, empty)
      val h3 = meld(h1,h2)
      b == findMin(deleteMin(h3))
    }
    else true
  }
  
  property("sorted-sequence-of-inserts-give-sorted-sequence-of-minimums-on-deletions") = forAll { (a: A, b: A, c: A) =>
    if (a < b && b < c) {
      c == findMin(deleteMin(deleteMin(insert(c, insert(b, insert(a, empty))))))
    }
    else true
  }

// Map generator
  lazy val genMap: Gen[Map[Int, Int]] = for {
    k <- arbitrary[Int]
    v <- arbitrary[Int]
    m <- oneOf(const(Map.empty[Int, Int]), genMap)
  } yield m.updated(k, v)

// Heap generator
  lazy val genHeap: Gen[H] = for {
    isEmpty <- arbitrary[Boolean]
    heap <- if (isEmpty) emptyHeaps else nonEmptyHeaps
  } yield heap
  
  def emptyHeaps = Gen.const(empty)
  
  def nonEmptyHeaps = for {
    elt <- arbitrary[A]
    heap <- genHeap
  } yield insert(elt, heap)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

}
