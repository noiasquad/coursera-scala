package kvstore

import akka.actor.{ ActorRef, Props, Actor, ActorLogging }
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import akka.actor.Actor.Receive
import akka.pattern.pipe
import scala.util.Success
import scala.util.Failure

/**
 * Retry Akka actor /Ask pattern with individual timeout, retry intervals
 */
object ReTry {
  private case class Retry(originalSender: ActorRef, message: Any, times: Int)

  private case class Response(originalSender: ActorRef, result: Any)

  case class RetryTimeout(message: Any, forwardTo: ActorRef)

  def props(tries: Int, retryTimeOut: FiniteDuration, retryInterval: FiniteDuration, forwardTo: ActorRef): Props =
    Props(new ReTry(tries: Int, retryTimeOut: FiniteDuration, retryInterval: FiniteDuration, forwardTo: ActorRef))
}

/**
 * Retry Akka actor /Ask pattern with individual timeout, retry intervals
 */
class ReTry(val tries: Int, retryTimeOut: FiniteDuration, retryInterval: FiniteDuration, forwardTo: ActorRef) extends Actor with ActorLogging {

  import context.dispatcher
  import ReTry._

  // Retry loop that keep on Re-trying the request
  def retryLoop: Receive = {

    // Response from future either Success or Failure is a Success - we propagate it back to a original sender
    case Response(originalSender, result) =>
      log.debug("return awaited response {} to original actor {}", result, originalSender);
      originalSender ! result
      context stop self

    case Retry(originalSender, message, triesLeft) =>
      log.debug("create a future for this request {}, try number {}", message, triesLeft);
      
      // Process (Re)try here. When future completes it sends result to self
      (forwardTo ? message)(retryTimeOut) onComplete {

        case Success(result) =>
          log.debug("future returned successful result {}", result);
          // sending responses via self synchronizes results from futures that may come potentially in any order.
          // It also helps the case when the actor is stopped (in this case responses will become dead-letters)
          self ! Response(originalSender, result)

        case Failure(ex) =>
          if (triesLeft - 1 == 0) { // In case of last try and we got a failure (timeout) lets send Retries exceeded error 
            log.debug("future returned a failure {}, no more tries left", ex);
            self ! Response(originalSender, RetryTimeout(message, forwardTo))
          }
      }

      // Send one more retry after interval
      if (triesLeft - 1 > 0)
        context.system.scheduler.scheduleOnce(retryInterval, self, Retry(originalSender, message, triesLeft - 1))

    case m @ _ =>
      log.error("No handling defined for message: " + m)

  }

  // Initial receive loop
  def receive: Receive = {

    case message @ _ =>
      self ! Retry(sender, message, tries)

      // Lets swap to a retry loop here.
      context.become(retryLoop, false)
  }

}