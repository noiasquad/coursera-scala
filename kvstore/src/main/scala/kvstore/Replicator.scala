package kvstore

import akka.actor.Props
import akka.actor.Actor
import akka.actor.ActorRef
import scala.concurrent.duration._

object Replicator {
  case class Replicate(key: String, valueOption: Option[String], id: Long)
  case class Replicated(key: String, id: Long)
  
  case class Snapshot(key: String, valueOption: Option[String], seq: Long)
  case class SnapshotAck(key: String, seq: Long)

  def props(replica: ActorRef): Props = Props(new Replicator(replica))
}

class Replicator(val replica: ActorRef) extends Actor with akka.actor.ActorLogging {
  import Replicator._
  import Replica._
  import context.dispatcher
  
  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  // map from sequence number to pair of sender and request
  var acks = Map.empty[Long, (ActorRef, Replicate)]
  // a sequence of not-yet-sent snapshots (you can disregard this if not implementing batching)
  var pending = Vector.empty[Snapshot]
  
  var _seqCounter = 0L
  def nextSeq = {
    val ret = _seqCounter
    _seqCounter += 1
    ret
  }
  
  /* TODO Behavior for the Replicator. */
  def receive: Receive = {
    case replicationRequest @ Replicate(key, value, id) => {
      log.debug("received replication request {}", replicationRequest)
      val snapshotId = nextSeq
      val retryActorRef = context.actorOf(ReTry.props(10, 1000.milliseconds, 100.milliseconds, replica), "retry-snapshot-" + snapshotId)
      retryActorRef ! Snapshot(key, value, snapshotId)
      context.become(trySnapshot(sender))
    }

    case _ =>
  }
  
  def trySnapshot(issuer: ActorRef): Receive = {
    case acknowledge @ SnapshotAck(key, seq) => {
      log.debug("received acknowledgement for snapshot {}", acknowledge)
      issuer ! Replicated(key, seq)
      context.become(receive)
    }

    case replicationRequest @ Replicate(key, value, id) => {
      log.debug("received another replication request {} during replicating", replicationRequest);
      self ! replicationRequest
    }

    case _ =>
  }
}
