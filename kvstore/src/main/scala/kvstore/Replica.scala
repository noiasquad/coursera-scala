package kvstore

import akka.actor.{ OneForOneStrategy, Props, ActorRef, Actor }
import kvstore.Arbiter._
import scala.collection.immutable.Queue
import akka.actor.SupervisorStrategy.Restart
import scala.annotation.tailrec
import akka.pattern.{ ask, pipe }
import akka.actor.Terminated
import scala.concurrent.duration._
import akka.actor.PoisonPill
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy
import akka.util.Timeout
import scala.util.Failure

object Replica {
  sealed trait Operation {
    def key: String
    def id: Long
  }
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long) extends Operation
  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply
  case class OperationAck(id: Long) extends OperationReply
  case class OperationFailed(id: Long) extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(new Replica(arbiter, persistenceProps))
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor with akka.actor.ActorLogging {
  import Replica._
  import Replicator._
  import Persistence._
  import ReTry._
  import context.dispatcher

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */
  override def preStart(): Unit = {
    arbiter ! Join
  }

  OneForOneStrategy() {
    case _: PersistenceException ⇒ Restart
  }

  var kv = Map.empty[String, String]
  // a map from secondary replicas to replicators
  var secondaries = Map.empty[ActorRef, ActorRef]
  // the current set of replicators
  var replicators = Set.empty[ActorRef]
  
  var persistence = context.actorOf(persistenceProps)

  var snapshotSeq = 0L;
  var replicationSeq = 0L;
  var replicatorSeq = 0L;

  def receive = {
    case JoinedPrimary   => context.become(leader)
    case JoinedSecondary => context.become(replica)
  }

  /* TODO Behavior for  the leader role. */
  val leader: Receive = {
    // KV protocol
    case updateRequest @ Insert(key, value, id) =>
      kv += key -> value;
      val retryActorRef = context.actorOf(ReTry.props(10, 1000.milliseconds, 100.milliseconds, persistence), "retry-update-" + id)
      retryActorRef ! Persist(key, Some(value), id)
      context.become(tryUpdate(id, sender))

    case updateRequest @ Remove(key, id) =>
      kv -= key;
      val retryActorRef = context.actorOf(ReTry.props(10, 1000.milliseconds, 100.milliseconds, persistence), "retry-update-" + id)
      retryActorRef ! Persist(key, None, id)
      context.become(tryUpdate(id, sender))

    case Get(key, id) =>
      sender ! GetResult(key, kv.get(key), id)

    // Replication protocol
    case replicationRequest @ Replicas(replicas) => {
      log.debug("received replication request {}", replicationRequest);
      replicas foreach (replica => {
        if (replica != self) {
          secondaries = secondaries.filterKeys { x => replicas.contains(x) }
          if (!secondaries.contains(replica)) {
            log.debug("register one secondary {}", replica);
            secondaries += replica -> context.actorOf(Replicator.props(replica), "replicator-" + replicatorSeq)
          }
          replicatorSeq += 1

          kv foreach ({
            case (key, value) => {
              val retryActorRef = context.actorOf(ReTry.props(1, 1000.milliseconds, 100.milliseconds, secondaries(replica)), "retry-replicate-" + replicationSeq)
              if (kv.contains(key))
                retryActorRef ! Replicate(key, Some(kv(key)), replicationSeq);
              else
                retryActorRef ! Replicate(key, None, replicationSeq);
              replicationSeq += 1;
            }
          })
        }
      })
    }

    case _ =>
  }

  def tryUpdate(updateId: Long, issuer: ActorRef): Receive = {
    // KV protocol
    case Get(key, id) => sender ! GetResult(key, kv.get(key), id)

    case updateRequest @ Insert(key, value, id) => {
      log.debug("received another update request {} during persisting", updateRequest);
      self ! updateRequest
    }

    case updateRequest @ Remove(key, id) => {
      log.debug("received another update request {} during persisting", updateRequest);
      self ! updateRequest
    }

    case RetryTimeout(Persist(_, _, id), _) => {
      log.debug("received retry timeout on local persistence {}, return failure to {}", id, issuer);
      issuer ! OperationFailed(id)
      context.become(leader)
    }

    // Replication protocol
    case replicationRequest @ Replicas(replicas) => {
      log.debug("received replication request when updating {}", replicationRequest);
      secondaries = secondaries.filterKeys { x => replicas.contains(x) }
      replicas foreach (replica => {
        if (replica != self) {
          if (!secondaries.contains(replica)) {
            log.debug("register one secondary {}", replica);
            secondaries += replica -> context.actorOf(Replicator.props(replica), "replicator-" + replicatorSeq)
            replicatorSeq += 1;
          }
        }
      })
    }

    case localPersistAcknowledge @ Persisted(key, id) => {
      log.debug("received acknowledgement for local persistence {}", localPersistAcknowledge);

      if (secondaries.isEmpty) {
        issuer ! OperationAck(id)
        context.become(leader)
      } else {
        log.debug("order replication on secondaries for {}", id);
        secondaries foreach ({
          case (replica, replicator) => {
            val retryActorRef = context.actorOf(ReTry.props(1, 1000.milliseconds, 100.milliseconds, replicator), "retry-replicate-" + replicationSeq)
            replicators += retryActorRef
            if (kv.contains(key))
              retryActorRef ! Replicate(key, Some(kv(key)), replicationSeq);
            else
              retryActorRef ! Replicate(key, None, replicationSeq);
            replicationSeq += 1;
          }
        })
        context.become(tryReplicate(id, issuer))
      }
    }

    case _ =>
  }

  def tryReplicate(updateId: Long, issuer: ActorRef): Receive = {
    // KV protocol
    case updateRequest @ Insert(key, value, id) => {
      log.debug("received an update request {} when replicating", updateRequest);
      self ! updateRequest
    }

    case updateRequest @ Remove(key, id) => {
      log.debug("received an update request {} when replicating", updateRequest);
      self ! updateRequest
    }

    // Replication protocol
    case replicationRequest @ Replicas(replicas) => {
      log.debug("received a replication request when replicating {}", replicationRequest);
      secondaries = secondaries.filterKeys { x => replicas.contains(x) }
      replicas foreach (replica => {
        if (replica != self) {
          if (!secondaries.contains(replica)) {
            log.debug("register one secondary {}", replica);
            secondaries += replica -> context.actorOf(Replicator.props(replica), "replicator-" + replicatorSeq)
            replicatorSeq += 1;
          }
        }
      })
    }

    case Replicated(_, id) => {
      log.debug("received acknowlegdement for replication {} from retry-replicator {}", id, sender);
      if (secondaries.isEmpty) {
        log.debug("no more secondaries to replicate, return acknowledge", id);
        issuer ! OperationAck(id)
        replicators = Set.empty
        context.become(leader)
      }
      else {
        replicators -= sender
        if (replicators.isEmpty) {
          log.debug("every replicator returned an answer for {}, return acknowledge", id);
          issuer ! OperationAck(id)
          context.become(leader)
        }
        else {
          log.debug("waiting for other replicators to reply for {}", id);
        }
      }
    }

    case RetryTimeout(Replicate(_, _, id), replicator) => {
      log.debug("received retry timeout on replication {}, return failure to {}", id, issuer)
      if (secondaries.isEmpty) {
        log.debug("do not fail on timeout for {} since no more secondaries, return acknowledge", id);
        issuer ! OperationAck(id)
        context.become(leader)
      }
      else if (secondaries.values.filter { x => x == replicator }.isEmpty) {
        log.debug("do not fail on timeout for {} since associated secondary is no more in secondaries", id);
      }
      else {
        log.debug("global replication failed since some secondary could not be replicated", id);
        issuer ! OperationFailed(id)
        context.become(leader)
      }
    }
  }

  /* TODO Behavior for the replica role. */
  val replica: Receive = {
    // KV protocol
    case Get(key, id) => sender ! GetResult(key, kv.get(key), id)

    // Replication protocol
    case snaphotRequest @ Snapshot(key, value, seq) => {
      log.debug("received snapshot request {}", snaphotRequest);

      if (seq > snapshotSeq) {
        // in the future, drop it without acknowledge
        log.debug("in the future, drop it without acknowledge {}", snaphotRequest);
      } else if (seq < snapshotSeq) {
        // in the past, drop it and acknowledge immediately
        log.debug("in the past, drop it and acknowledge immediately {}", snaphotRequest);
        sender ! SnapshotAck(key, seq)
      } else {
        // expected, store it and acknowledge
        log.debug("snapshot request {} from {} accepted, value stored in local store and persist request sent", snaphotRequest, sender);

        if (!value.isEmpty)
          kv += key -> (value.get)
        else
          kv -= key;

        snapshotSeq += 1;

        val retryActorRef = context.actorOf(ReTry.props(10, 1000.milliseconds, 100.milliseconds, persistence))
        retryActorRef ! Persist(key, value, seq)
        context.become(trySnapshot(seq, sender))
      }
    }

    case _ =>
  }

  def trySnapshot(snapshotId: Long, issuer: ActorRef): Receive = {
    // KV protocol
    case Get(key, id) => sender ! GetResult(key, kv.get(key), id)

    // Replication protocol
    case persistAck @ Persisted(key, id) => {
      val snapshotAck = SnapshotAck(key, snapshotId)
      log.debug("received acknowledgement {} and send {} to {}", persistAck, snapshotAck, issuer);
      issuer ! snapshotAck
      context.become(replica)
    }

    case snapshotRequest @ Snapshot(key, value, seq) => {
      //TODO
      log.debug("received another snapshot request {} during persisting", snapshotRequest);
    }

    case RetryTimeout(_, _) =>
    case _                  =>
  }

}

