package funsets

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 *  - run the "test" command in the SBT console
 *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {


  /**
   * Link to the scaladoc - very clear and detailed tutorial of FunSuite
   *
   * http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite
   *
   * Operators
   *  - test
   *  - ignore
   *  - pending
   */

  /**
   * Tests are written using the "test" operator and the "assert" method.
   */
  test("string take") {
    val message = "hello, world"
    assert(message.take(5) == "hello")
  }

  /**
   * For ScalaTest tests, there exists a special equality operator "===" that
   * can be used inside "assert". If the assertion fails, the two values will
   * be printed in the error message. Otherwise, when using "==", the test
   * error message will only say "assertion failed", without showing the values.
   *
   * Try it out! Change the values so that the assertion fails, and look at the
   * error message.
   */
  test("adding ints") {
    assert(1 + 2 === 3)
  }

  
  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }
  
  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   * 
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   * 
   *   val s1 = singletonSet(1)
   * 
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   * 
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   * 
   */

  trait TestSets {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
    val naturals: Set = (x: Int) => x >= 0
    val naturalsUntil10: Set = (x: Int) => x >= 0 && x <= 10
    val someNaturals: Set = (x: Int) => x == 2 || x == 4 || x == 6 || x == 9 || x == 11
  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   * 
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSet(1) contains 1") {
    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3". 
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "Singleton")
    }
  }

  test("union1") {
    new TestSets {
      val s = union(s1, s2)
      assert(contains(s, 1), "Union 1")
      assert(contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
    }
  }

  test("union2") {
    new TestSets {
      val s6 = union(s1, s1)
      assert(contains(s6, 1), "Union 1")
      assert(!contains(s6, 2), "Union 2")
      assert(!contains(s6, 3), "Union 3")
    }
  }

  test("union3") {
    new TestSets {
      val s7 = union(s1, emptySet)
      assert(contains(s7, 1), "Union 1")
      assert(!contains(s7, 2), "Union 2")
      assert(!contains(s7, 3), "Union 3")
    }
  }

  test("union4") {
    new TestSets {
  	  val s5 = union(naturalsUntil10, someNaturals)
  	  assert(contains(s5, 0))
  	  assert(contains(s5, 1))
  	  assert(contains(s5, 2))
  	  assert(contains(s5, 3))
  	  assert(contains(s5, 4))
  	  assert(contains(s5, 5))
  	  assert(contains(s5, 6))
  	  assert(contains(s5, 7))
  	  assert(contains(s5, 8))
  	  assert(contains(s5, 9))
  	  assert(contains(s5, 10))
  	  assert(contains(s5, 11))
    }
  }

  test("intersect") {
    new TestSets {      
  	  val s4= intersect(naturalsUntil10, someNaturals)
  	  assert(contains(s4, 2))
  	  assert(contains(s4, 4))
  	  assert(contains(s4, 6))
  	  assert(contains(s4, 9))
  	  assert(!contains(s4, 11))
    }
  }

  test("diff") {
    new TestSets {      
  	  val s6 = diff(naturalsUntil10, someNaturals)
  	  assert(contains(s6, 0))
  	  assert(contains(s6, 1))
  	  assert(!contains(s6, 2))
  	  assert(contains(s6, 3))
  	  assert(!contains(s6, 4))
  	  assert(contains(s6, 5))
  	  assert(!contains(s6, 6))
  	  assert(contains(s6, 7))
  	  assert(contains(s6, 8))
  	  assert(!contains(s6, 9))
  	  assert(contains(s6, 10))
  	  assert(!contains(s6, 11))
    }
  }

  test("filter1") {
    new TestSets {      
  	  val s7 = filter(naturalsUntil10, (x: Int) => x % 2 == 0)
  	  assert(contains(s7, 0))
  	  assert(!contains(s7, 1))
  	  assert(contains(s7, 2))
  	  assert(!contains(s7, 3))
  	  assert(contains(s7, 4))
  	  assert(!contains(s7, 5))
  	  assert(contains(s7, 6))
  	  assert(!contains(s7, 7))
  	  assert(contains(s7, 8))
  	  assert(!contains(s7, 9))
  	  assert(contains(s7, 10))
    }
  }

  test("filter2") {
    new TestSets {      
  	  val s8 = filter(naturalsUntil10, (x: Int) => x % 2 != 0)
  	  assert(!contains(s8, 0))
  	  assert(contains(s8, 1))
  	  assert(!contains(s8, 2))
  	  assert(contains(s8, 3))
  	  assert(!contains(s8, 4))
  	  assert(contains(s8, 5))
  	  assert(!contains(s8, 6))
  	  assert(contains(s8, 7))
  	  assert(!contains(s8, 8))
  	  assert(contains(s8, 9))
  	  assert(!contains(s8, 10))
    }
  }

  test("filter3") {
    new TestSets {      
  	  val s9 = filter(naturalsUntil10, (x: Int) => x % 2 == 0 && x % 3 == 0)
  	  assert(contains(s9, 0))
  	  assert(!contains(s9, 1))
  	  assert(!contains(s9, 2))
  	  assert(!contains(s9, 3))
  	  assert(!contains(s9, 4))
  	  assert(!contains(s9, 5))
  	  assert(contains(s9, 6))
  	  assert(!contains(s9, 7))
  	  assert(!contains(s9, 8))
  	  assert(!contains(s9, 9))
  	  assert(!contains(s9, 10))
    }
  }

  test("exists") {
    new TestSets {      
  	  assert(exists(naturalsUntil10, (x: Int) => x % 2 == 0 && x % 3 == 0))
    }
  }

  test("map1") {
    new TestSets {      
	  val s10 = map(naturalsUntil10, (x: Int) => x * 2)
  	  assert(contains(s10, 0))
  	  assert(contains(s10, 2))
  	  assert(contains(s10, 4))
  	  assert(contains(s10, 6))
  	  assert(contains(s10, 8))
  	  assert(contains(s10, 10))
  	  assert(contains(s10, 12))
  	  assert(contains(s10, 14))
  	  assert(contains(s10, 16))
  	  assert(contains(s10, 18))
  	  assert(contains(s10, 20))
    }
  }

  test("map2") {
    new TestSets {      
  	  val s11 = map(naturalsUntil10, (x: Int) => x * x)
  	  assert(contains(s11, 0))
  	  assert(contains(s11, 1))
  	  assert(contains(s11, 4))
  	  assert(contains(s11, 9))
  	  assert(contains(s11, 16))
  	  assert(contains(s11, 25))
  	  assert(contains(s11, 36))
  	  assert(contains(s11, 49))
  	  assert(contains(s11, 64))
  	  assert(contains(s11, 81))
  	  assert(contains(s11, 100))
    }
  }
}
