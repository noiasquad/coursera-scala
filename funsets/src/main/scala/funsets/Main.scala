package funsets

object Main extends App {
  import FunSets._

  println(contains(singletonSet(1), 1))
  printSet(singletonSet(1))
  printSet(union(singletonSet(1), singletonSet(2)))
  printSet(union(singletonSet(1), singletonSet(1)))
  printSet(emptySet)
  printSet(union(singletonSet(1), emptySet))
  printSet(intersect(singletonSet(1), emptySet))
  printSet(intersect(singletonSet(1), singletonSet(1)))
  printSet(intersect(singletonSet(1), singletonSet(2)))
  
  val naturalsUntil10:Set = filter(naturalNumbersSet, (x: Int) => x <= 10)
  val someNaturals:Set = filter(naturalNumbersSet, (x: Int) => x == 2 || x == 4 || x == 6 || x == 9 || x == 11)
  
  printSet(intersect(naturalsUntil10, someNaturals))
  printSet(union(naturalsUntil10, someNaturals))
  printSet(diff(naturalsUntil10, someNaturals))
  
  printSet(filter(naturalsUntil10, (x: Int) => x % 2 == 0))
  printSet(filter(naturalsUntil10, (x: Int) => x % 2 != 0))
  
  println(exists(naturalsUntil10, (x: Int) => x % 2 == 0 && x % 3 == 0))
  printSet(filter(naturalsUntil10, (x: Int) => x % 2 == 0 && x % 3 == 0))
  
  printSet(map(naturalsUntil10, (x: Int) => x * 2))
  printSet(map(naturalsUntil10, (x: Int) => x * x))
}
