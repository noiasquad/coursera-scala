package funsets

object funsetsws {
  type Set = Int => Boolean

  def contains(s: Set, elem: Int): Boolean = s(elem)
                                                  //> contains: (s: Int => Boolean, elem: Int)Boolean

  def singletonSet(elem: Int): Set = (x: Int) => x == elem
                                                  //> singletonSet: (elem: Int)Int => Boolean
  contains(singletonSet(1), 9)                    //> res0: Boolean = false
  contains(singletonSet(1), 1)                    //> res1: Boolean = true

  def union(s: Set, t: Set): Set = (x: Int) => contains(s, x) || contains(t, x)
                                                  //> union: (s: Int => Boolean, t: Int => Boolean)Int => Boolean
  def intersect(s: Set, t: Set): Set = (x: Int) => contains(s, x) && contains(t, x)
                                                  //> intersect: (s: Int => Boolean, t: Int => Boolean)Int => Boolean
  def diff(s: Set, t: Set): Set = (x: Int) => contains(s, x) && !contains(t, x)
                                                  //> diff: (s: Int => Boolean, t: Int => Boolean)Int => Boolean
  def filter(s: Set, p: Int => Boolean): Set = (x: Int) => contains(s, x) && p(x)
                                                  //> filter: (s: Int => Boolean, p: Int => Boolean)Int => Boolean

	def forall1(s: Set, p: Int => Boolean): (Int, Int) => Boolean = {
			def forallF(min: Int, max: Int) : Boolean =
	      if (min > max) true
	      else if (contains(s,min)) p(min) && forallF(min+1, max)
	      else forallF(min+1, max)
	    forallF
	}                                         //> forall1: (s: Int => Boolean, p: Int => Boolean)(Int, Int) => Boolean
  
  def forall2(s: Set, p: Int => Boolean)(min: Int, max: Int): Boolean =
      if (min > max) true
      else if (contains(s,min)) p(min) && forall2(s, p)(min+1, max)
      else forall2(s,p)(min+1, max)               //> forall2: (s: Int => Boolean, p: Int => Boolean)(min: Int, max: Int)Boolean
  
  def mapReduce(s:Set, p: Int => Boolean, combine: (Boolean, Boolean) => Boolean, zero: Boolean)(min: Int, max: Int): Boolean =
  	if (min > max) zero
  	else if (contains(s,min)) combine(p(min), mapReduce(s, p, combine, zero)(min+1, max))
  	else mapReduce(s, p, combine, zero)(min+1, max)
                                                  //> mapReduce: (s: Int => Boolean, p: Int => Boolean, combine: (Boolean, Boolea
                                                  //| n) => Boolean, zero: Boolean)(min: Int, max: Int)Boolean
  
  def forall3(s: Set, p: Int => Boolean)(min: Int, max: Int): Boolean = mapReduce(s, p, (x,y) => x && y, true)(min, max)
                                                  //> forall3: (s: Int => Boolean, p: Int => Boolean)(min: Int, max: Int)Boolean
 
  def forall(s: Set, p: Int => Boolean): Boolean = forall1(s,p)(-1000,1000)
                                                  //> forall: (s: Int => Boolean, p: Int => Boolean)Boolean
  
  def exists(s: Set, p: Int => Boolean): Boolean = !forall1(s, x => !p(x))(-1000,1000)
                                                  //> exists: (s: Int => Boolean, p: Int => Boolean)Boolean
  	
  def map(s: Set, f: Int => Int): Set = (y: Int) => exists(s, x => y == f(x))
                                                  //> map: (s: Int => Boolean, f: Int => Int)Int => Boolean

  val emptySet: Set = (Int) => false              //> emptySet  : Int => Boolean = <function1>
  val naturalNumbersSet: Set = (x: Int) => x >= 0 //> naturalNumbersSet  : Int => Boolean = <function1>
  
  forall3(naturalNumbersSet, x => x > 0)(1,10)    //> res2: Boolean = true
}