package recfun

object balancews {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  def balance(chars: List[Char]): Boolean = {
  	def bal(chars: List[Char], p: Int): Boolean =
  		if (chars.isEmpty) p == 0
  		else if (p < 0) false
  		else if (chars.head == '(') bal(chars.tail, p+1)
  		else if (chars.head == ')') bal(chars.tail, p-1)
  		else bal(chars.tail, p)
  		
  	bal(chars,0)
  }                                               //> balance: (chars: List[Char])Boolean

	balance(":-)".toList)                     //> res0: Boolean = false
	balance("(:-)".toList)                    //> res1: Boolean = true
	balance("(:(-))".toList)                  //> res2: Boolean = true
	balance("((:-)".toList)                   //> res3: Boolean = false
	balance("(123)()".toList)                 //> res4: Boolean = true
	balance("(if(zero? x) max (/ 1 x))".toList)
                                                  //> res5: Boolean = true
	balance("I told him (that it''s not (yet) done). (But he wasn''t listening)".toList)
                                                  //> res6: Boolean = true
  balance("".toList)                              //> res7: Boolean = true
  balance("())(".toList)                          //> res8: Boolean = false
  balance("())(((((((((((((((((((((((((((((((((((((((((((((".toList)
                                                  //> res9: Boolean = false
}