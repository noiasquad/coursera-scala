package recfun
import common._
import java.security.InvalidParameterException

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int =
    if (r < 0) throw new IllegalArgumentException("row")
    else if (c < 0) throw new IllegalArgumentException("column")
    else if (r == 0) 1
    else if (c == 0) 1
    else if (c == r) 1
    else pascal(c - 1, r - 1) + pascal(c, r - 1)

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    def bal(chars: List[Char], p: Int): Boolean =
      if (chars.isEmpty) p == 0
      else if (p < 0) false
      else if (chars.head == '(') bal(chars.tail, p + 1)
      else if (chars.head == ')') bal(chars.tail, p - 1)
      else bal(chars.tail, p)
    bal(chars, 0)
  }
  
  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    def useCoins(money: Int, coins: List[Int]): Int =
    	if (money == 0) 1
    	else if (coins.isEmpty) 0
    	else useCoin(money, coins.head, money/coins.head, coins.tail)

  	def useCoin(money: Int, coin: Int, occurences: Int, otherCoins: List[Int]): Int =
  		if (occurences == 0) useCoins(money, otherCoins)
  		else if (money-coin*occurences == 0) 1 + useCoin(money, coin, occurences-1, otherCoins)
  		else useCoins(money-coin*occurences, otherCoins) + useCoin(money, coin, occurences-1, otherCoins)

  	if (money<0) throw new IllegalArgumentException("money")
  	else useCoins(money, coins)
  }
}
