package recfun

object changews {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet

  def countChange(money: Int, coins: List[Int]): Int = {
  
    def useCoins(money: Int, coins: List[Int]): Int =
    	if (money == 0) 1
    	else if (coins.isEmpty) 0
    	else useCoin(money, coins.head, money/coins.head, coins.tail)

  	def useCoin(money: Int, coin: Int, occurences: Int, otherCoins: List[Int]): Int =
  		if (occurences == 0) useCoins(money, otherCoins)
  		else if (money-coin*occurences == 0) 1 + useCoin(money, coin, occurences-1, otherCoins)
  		else useCoins(money-coin*occurences, otherCoins) + useCoin(money, coin, occurences-1, otherCoins)

    useCoins(money, coins)
	}                                         //> countChange: (money: Int, coins: List[Int])Int
	countChange(4,List(1,2))                  //> res0: Int = 3
	countChange(300,List(5,10,20,50,100,200,500))
                                                  //> res1: Int = 1022
	countChange(301,List(5,10,20,50,100,200,500))
                                                  //> res2: Int = 0
	countChange(300,List(500,5,50,100,20,200,10))
                                                  //> res3: Int = 1022
	countChange(4,List())                     //> res4: Int = 0
	countChange(0,List())                     //> res5: Int = 1
	countChange(0,List(1))                    //> res6: Int = 1
}