package wikipedia

import org.scalatest.{FunSuite, BeforeAndAfterAll}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class WikipediaSuite extends FunSuite with BeforeAndAfterAll {

  def initializeWikipediaRanking(): Boolean =
    try {
      WikipediaRanking
      true
    } catch {
      case ex: Throwable =>
        println(ex.getMessage)
        ex.printStackTrace()
        false
    }

  override def afterAll(): Unit = {
    assert(initializeWikipediaRanking(), " -- did you fill in all the values in WikipediaRanking (conf, sc, wikiRdd)?")
    import WikipediaRanking._
    sc.stop()
  }

  // Conditions:
  // (1) the language stats contain the same elements
  // (2) they are ordered (and the order doesn't matter if there are several languages with the same count)
  def assertEquivalentAndOrdered(given: List[(String, Int)], expected: List[(String, Int)]): Unit = {
    // (1)
    assert(given.toSet == expected.toSet, "The given elements are not the same as the expected elements")
    // (2)
    assert(
      !(given zip given.tail).exists({ case ((_, occ1), (_, occ2)) => occ1 < occ2 }),
      "The given elements are not in descending order"
    )
  }

  test("'occurrencesOfLang' should work for (specific) RDD with one element") {
    assert(initializeWikipediaRanking(), " -- did you fill in all the values in WikipediaRanking (conf, sc, wikiRdd)?")
    import WikipediaRanking._
    val rdd = sc.parallelize(Seq(WikipediaArticle("title", "Java Jakarta")))
    assert(occurrencesOfLang("Java", rdd) == 1)
  }

  test("'occurrencesOfLang.empty_rdd'") {
    assert(initializeWikipediaRanking(), " -- did you fill in all the values in WikipediaRanking (conf, sc, wikiRdd)?")
    import WikipediaRanking._
    val rdd = sc.parallelize(List(WikipediaArticle("any title","")))
    assert(occurrencesOfLang("scala", rdd) == 0)
  }

  test("'occurrencesOfLang.more_articles'") {
    assert(initializeWikipediaRanking(), " -- did you fill in all the values in WikipediaRanking (conf, sc, wikiRdd)?")
    import WikipediaRanking._
    val articles = List(
      WikipediaArticle("1","Scala JavaScript"),
      WikipediaArticle("2","Java JAVA JavaScript Scala"),
      WikipediaArticle("3","Scala Scala")
    )
    val rdd = sc.parallelize(articles)
    assert(occurrencesOfLang("Scala", rdd) == 3)
    assert(occurrencesOfLang("JavaScript", rdd) == 2)
    assert(occurrencesOfLang("Java", rdd) == 1)
    assert(occurrencesOfLang("Groovy", rdd) == 0)
    assert(occurrencesOfLang("Erlang", rdd) == 0)
    assert(occurrencesOfLang("Haskell", rdd) == 0)
  }

  test("'rankLangs' should work for RDD with two elements") {
    assert(initializeWikipediaRanking(), " -- did you fill in all the values in WikipediaRanking (conf, sc, wikiRdd)?")
    import WikipediaRanking._
    val langs = List("Scala", "Java")
    val rdd = sc.parallelize(List(
      WikipediaArticle("1", "Scala is great"),
      WikipediaArticle("2", "Java is OK, but Scala is cooler")))
    val ranked = rankLangs(langs, rdd)
    assert(ranked.head._1 == "Scala")
  }

  test("'rankLangs.empty_rdd'") {
    assert(initializeWikipediaRanking(), " -- did you fill in all the values in WikipediaRanking (conf, sc, wikiRdd)?")
    import WikipediaRanking._
    val rdd = sc.parallelize(List(WikipediaArticle("any title","")))
    val ranked = rankLangs(langs, rdd)
    assert(ranked.size == langs.length)
    assert(ranked(0) == (langs(0), 0))
    assert(ranked(langs.size-1) == (langs.last, 0))
  }

  test("'rankLangs.more_articles'") {
    assert(initializeWikipediaRanking(), " -- did you fill in all the values in WikipediaRanking (conf, sc, wikiRdd)?")
    import WikipediaRanking._
    val langs = List("JavaScript", "Java", "Scala", "Haskell", "Groovy")
    val articles = List(
      WikipediaArticle("1","Scala JavaScript"),
      WikipediaArticle("2","Java JAVA JavaScript"),
      WikipediaArticle("3","Scala Scala"),
      WikipediaArticle("4","Groovy Scala"),
      WikipediaArticle("5","Scala")
    )
    val rdd = sc.parallelize(articles)
    val ranked = rankLangs(langs, rdd)
    assert(ranked.size == langs.length)
    assert(ranked(0) == ("Scala", 4))
    assert(ranked(1) == ("JavaScript", 2))
    assert(ranked(2) == ("Java", 1))
    assert(ranked(3) == ("Groovy", 1))
    assert(ranked(4) == ("Haskell", 0))
  }

  test("'rankLangs.bigdata'") {
    assert(initializeWikipediaRanking(), " -- did you fill in all the values in WikipediaRanking (conf, sc, wikiRdd)?")
    import WikipediaRanking._
    val ranked = rankLangs(langs, wikiRdd)
    println(ranked)
  }

  test("'makeIndex' creates a simple index with two entries") {
    assert(initializeWikipediaRanking(), " -- did you fill in all the values in WikipediaRanking (conf, sc, wikiRdd)?")
    import WikipediaRanking._
    val langs = List("Scala", "Java")
    val articles = List(
      WikipediaArticle("1","Groovy is pretty interesting, and so is Erlang"),
      WikipediaArticle("2","Scala and Java run on the JVM"),
      WikipediaArticle("3","Scala is not purely functional")
    )
    val rdd = sc.parallelize(articles)
    val index = makeIndex(langs, rdd)
    assert(index.count() == 2)
  }

  test("'makeIndex.minimal'") {
    assert(initializeWikipediaRanking(), " -- did you fill in all the values in WikipediaRanking (conf, sc, wikiRdd)?")
    import WikipediaRanking._
    val langs = List("JavaScript", "Java", "Scala", "Erlang", "Groovy")
    val articles = List(
      WikipediaArticle("1","Groovy is pretty interesting, and so is Erlang"),
      WikipediaArticle("2","Scala and Java run on the JVM"),
      WikipediaArticle("3","Scala is not purely functional")
    )
    val rdd = sc.parallelize(articles)
    val indexRdd = makeIndex(langs, rdd)
    val indexMap = indexRdd.collectAsMap()
    assert(indexMap.size == 4)
    assert(indexMap("Scala").size == 2)
    assert(indexMap("Java").size == 1)
    assert(indexMap("Groovy").size == 1)
    assert(indexMap("Erlang").size == 1)
  }

  test("'rankLangsUsingIndex' should work for a simple RDD with three elements") {
    assert(initializeWikipediaRanking(), " -- did you fill in all the values in WikipediaRanking (conf, sc, wikiRdd)?")
    import WikipediaRanking._
    val langs = List("Scala", "Java")
    val articles = List(
        WikipediaArticle("1","Groovy is pretty interesting, and so is Erlang"),
        WikipediaArticle("2","Scala and Java run on the JVM"),
        WikipediaArticle("3","Scala is not purely functional")
      )
    val rdd = sc.parallelize(articles)
    val index = makeIndex(langs, rdd)
    val ranked = rankLangsUsingIndex(index)
    val res = (ranked.head._1 == "Scala")
    assert(res)
  }

  test("'rankLangsReduceByKey' should work for a simple RDD with four elements") {
    assert(initializeWikipediaRanking(), " -- did you fill in all the values in WikipediaRanking (conf, sc, wikiRdd)?")
    import WikipediaRanking._
    val langs = List("Scala", "Java", "Groovy", "Haskell", "Erlang")
    val articles = List(
        WikipediaArticle("1","Groovy is pretty interesting, and so is Erlang"),
        WikipediaArticle("2","Scala and Java run on the JVM"),
        WikipediaArticle("3","Scala is not purely functional"),
        WikipediaArticle("4","The cool kids like Haskell more than Java"),
        WikipediaArticle("5","Java is for enterprise developers")
      )
    val rdd = sc.parallelize(articles)
    val ranked = rankLangsReduceByKey(langs, rdd)
    assert(ranked.head._1 == "Java")
  }

  test("'rankLangsReduceByKey.more_articles'") {
    assert(initializeWikipediaRanking(), " -- did you fill in all the values in WikipediaRanking (conf, sc, wikiRdd)?")
    import WikipediaRanking._
    val langs = List("JavaScript", "Java", "Scala", "Haskell", "Groovy")
    val articles = List(
      WikipediaArticle("1","Scala JavaScript"),
      WikipediaArticle("2","Java JAVA JavaScript"),
      WikipediaArticle("3","Scala Scala"),
      WikipediaArticle("4","Groovy Scala"),
      WikipediaArticle("5","Scala")
    )
    val rdd = sc.parallelize(articles)
    val ranked = rankLangsReduceByKey(langs, rdd)
    assert(ranked(0) == ("Scala", 4))
    assert(ranked(1) == ("JavaScript", 2))
    assert(ranked(2) == ("Java", 1))
    assert(ranked(3) == ("Groovy", 1))
  }
}
