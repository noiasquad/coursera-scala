package objsets

object livetestws {

TweetReader.tweetSets.head count                  //> res0: Int = 100

TweetReader.tweetSets.tail.head count             //> res1: Int = 100

TweetReader.tweetSets.tail.tail.head count        //> res2: Int = 100

TweetReader.tweetSets.tail.tail.tail.head count   //> res3: Int = 100

TweetReader.tweetSets.tail.tail.tail.tail.head count
                                                  //> res4: Int = 95

TweetReader.tweetSets.tail.tail.tail.tail.tail.head count
                                                  //> res5: Int = 100

TweetReader.tweetSets.tail.tail.tail.tail.tail.tail.head count
                                                  //> res6: Int = 100
                                                  
val allT = TweetReader.tweetSets.head union TweetReader.tweetSets.tail.head union TweetReader.tweetSets.tail.tail.head union TweetReader.tweetSets.tail.tail.tail.head union TweetReader.tweetSets.tail.tail.tail.tail.head union TweetReader.tweetSets.tail.tail.tail.tail.tail.head union TweetReader.tweetSets.tail.tail.tail.tail.tail.tail.head
                                                  //> allT  : objsets.TweetSet = {{{{{{{{{{{{{."It's now a surprise to hear of a c
                                                  //| ompany listening to its users, issuing an apology and vowing to make it bett
                                                  //| er." (http://t.co/R5bu5Wp1) [49].}"We've updated every aspect of iPhone 5,? 
                                                  //| @PSchiller says. #Apple #iPhone5 LIVEBLOG http://t.co/DwtKQkSu [16].}#Apple 
                                                  //| #iPhone5 battery: 8hrs of 3G talk time, 8hrs of LTE or 3G browsing, 10hrs of
                                                  //|  WiFi browsing, 225hrs of standby http://t.co/DwtKQkSu [121].}#Apple #iPhone
                                                  //| 5 camera is 8megapixels...same as iPhone 4S. But it's not the same camera ht
                                                  //| tp://t.co/DwtKQkSu #LIVEBLOG [30].}#Apple #iPhone5 comes in either Slate (ak
                                                  //| a black) or White http://t.co/DwtKQkSu LIVEBLOG [17]{.#Apple #iPhone5 has 40
                                                  //| % faster photo capture thanks iPhone 4S http://t.co/DwtKQkSu LIVEBLOG [17]{.
                                                  //| #Apple #iPhone5 photos up on our liveblog ---&gt; http://t.co/DwtKQkSu [35]{
                                                  //| {.#Apple CEO Tim Cook takes the stage at #iPhone5 event, has "really cool st
                                                  //| uff to show you.? http:/
                                                  //| Output exceeds cutoff limit.
allT count                                        //> res7: Int = 695

allT mostRetweeted                                //> res8: objsets.Tweet = 'Dexter' Season 7 Premiere Available in Full on @YouTu
                                                  //| be [VIDEO] http://t.co/Cc5HeiZZ [345]

allT filter(tweet => tweet.retweets == 345)       //> res9: objsets.TweetSet = {.'Dexter' Season 7 Premiere Available in Full on @
                                                  //| YouTube [VIDEO] http://t.co/Cc5HeiZZ [345].}

allT filter(tweet => tweet.text.contains("'Dexter' Season 7 Premiere"))
                                                  //> res10: objsets.TweetSet = {.'Dexter' Season 7 Premiere Available in Full on 
                                                  //| @YouTube [VIDEO] http://t.co/Cc5HeiZZ [345].}

  val google = List("android", "Android", "galaxy", "Galaxy", "nexus", "Nexus")
                                                  //> google  : List[String] = List(android, Android, galaxy, Galaxy, nexus, Nexu
                                                  //| s)
  val apple = List("ios", "iOS", "iphone", "iPhone", "ipad", "iPad")
                                                  //> apple  : List[String] = List(ios, iOS, iphone, iPhone, ipad, iPad)

val googleTweets: TweetSet = allT filter ((tweet: Tweet) => google.exists((keyword: String) => tweet.text.contains(keyword)))
                                                  //> googleTweets  : objsets.TweetSet = {.5 Mobile Photographers Capturing the W
                                                  //| orld With #Android http://t.co/786NneBt [78]{.7 Free #Android Apps for Kill
                                                  //| ing Time in Lines http://t.co/eKu5hhsh [42]{.A mathematician accurately pre
                                                  //| dicted when Android's app store would hit 25 billion downloads http://t.co/
                                                  //| VFLBJ0z3 [36]{.AT&amp;T 4G LTE adds Galaxy Note 2, Galay Tab 2 10.1, Galaxy
                                                  //|  Express and Galaxy Rugby Pro to lineup -  http://t.co/uvBFFMQO [12]{.BlueS
                                                  //| tacks and AMD Bring 500,000 Android Apps to Windows 8: http://t.co/GskuXhRo
                                                  //|  by @alexandra_chang [22]{.Camera contest:  Apple iPhone 5 vs. Samsung Gala
                                                  //| xy S3 vs. HTC One X http://t.co/PmbhNgrd [49]{.Court of Appeals for the Fed
                                                  //| eral Circuit tells Judge Koh to revisit Galaxy Tab 10.1 injunction -  http:
                                                  //| //t.co/iIOCcwDW [13]{.Cubify lets you skin, 3D print your own personal Andr
                                                  //| oid -  http://t.co/S6nimh5R [23]{.FAVI's $50 Streaming Stick adds apps, str
                                                  //| eaming services to any 
                                                  //| Output exceeds cutoff limit.

googleTweets count                                //> res11: Int = 38

val appleTweets: TweetSet = TweetReader.allTweets filter ((tweet: Tweet) => apple.exists((keyword: String) => tweet.text.contains(keyword)))
                                                  //> appleTweets  : objsets.TweetSet = {.18 unlucky people who already broke the
                                                  //|  iPhone 5: http://t.co/9RpvX4te [79]{.A week with the iPhone 5: http://t.co
                                                  //| /ReuK1aJs [111]{.Don't buy cheap iPhone 5 cables because they don't actuall
                                                  //| y exist yet http://t.co/3LHLeCdO [56]{.Eric Schmidt confirms a Google Maps 
                                                  //| app on iOS 6 is still some way off: http://t.co/bobRuY06 [76]{.How to build
                                                  //|  an iPhone 5 dock for $1.27 http://t.co/kqsQ1GIV [65]{.Is your iPhone 5 cam
                                                  //| era seeing purple? Like, where it shouldn't be? http://t.co/EBnaMfFR [37]{.
                                                  //| Is your iPhone 5... rattling? http://t.co/mn0r2dhb [67]{.Is your new iPhone
                                                  //|  picking up more scratches than you'd like? http://t.co/DGEiawOi [35]{{.The
                                                  //|  definitive comparison of iOS 5 Google Maps vs iOS 6 Apple Maps in one sing
                                                  //| le image: http://t.co/fTwTfVMy [191]{.The iPhone 5 'shortage' is apparently
                                                  //|  a result of its new ultra-thin display: http://t.co/RqUSuYif [48]{.The wei
                                                  //| rdest thing people hate
                                                  //| Output exceeds cutoff limit.

appleTweets count                                 //> res12: Int = 17

val trendyTweets = googleTweets union appleTweets //> trendyTweets  : objsets.TweetSet = {.18 unlucky people who already broke th
                                                  //| e iPhone 5: http://t.co/9RpvX4te [79]{{.5 Mobile Photographers Capturing th
                                                  //| e World With #Android http://t.co/786NneBt [78]{.7 Free #Android Apps for K
                                                  //| illing Time in Lines http://t.co/eKu5hhsh [42]{.A mathematician accurately 
                                                  //| predicted when Android's app store would hit 25 billion downloads http://t.
                                                  //| co/VFLBJ0z3 [36].}}}A week with the iPhone 5: http://t.co/ReuK1aJs [111]{{.
                                                  //| AT&amp;T 4G LTE adds Galaxy Note 2, Galay Tab 2 10.1, Galaxy Express and Ga
                                                  //| laxy Rugby Pro to lineup -  http://t.co/uvBFFMQO [12]{.BlueStacks and AMD B
                                                  //| ring 500,000 Android Apps to Windows 8: http://t.co/GskuXhRo by @alexandra_
                                                  //| chang [22]{.Camera contest:  Apple iPhone 5 vs. Samsung Galaxy S3 vs. HTC O
                                                  //| ne X http://t.co/PmbhNgrd [49]{.Court of Appeals for the Federal Circuit te
                                                  //| lls Judge Koh to revisit Galaxy Tab 10.1 injunction -  http://t.co/iIOCcwDW
                                                  //|  [13]{.Cubify lets you 
                                                  //| Output exceeds cutoff limit.
                                                  
trendyTweets mostRetweeted                        //> res13: objsets.Tweet = iPhone 5's brain dissected. Guess what, it's made by
                                                  //|  Samsung. http://t.co/wSyjvpDc [321]

val trendyList = trendyTweets descendingByRetweet //> trendyList  : objsets.TweetList = objsets.Cons@44d2be98

val allTweets: TweetSet = TweetReader.allTweets   //> allTweets  : objsets.TweetSet = {{{{{{.(In case you're wondering who the aw
                                                  //| esome speller is, that's @brentrose. He has an MFA.) [2]{.10 stupid, crazy,
                                                  //|  wonderful gadget fails http://t.co/p3Al28G5 [32].}}12 deadly inventions th
                                                  //| at killed their creators (not for the faint of heart) http://t.co/BCwRAzhe 
                                                  //| [82].}14 people who should be ashamed of their @foursquare mayorship http:/
                                                  //| /t.co/7AEHQJLT [50]{.18 places carbon fiber just doesn't belong: http://t.c
                                                  //| o/vJo1Yhlj [21].}}18 unlucky people who already broke the iPhone 5: http://
                                                  //| t.co/9RpvX4te [79]{{{{.A week with the iPhone 5: http://t.co/ReuK1aJs [111]
                                                  //| {.A woman had a new ear grown on her arm and attached to her head and it is
                                                  //|  PRETTY GROSS: http://t.co/NyuikEmP [88]{.Aboard the ligher, smarter, deadl
                                                  //| ier aircraft carrier of tomorrow http://t.co/xVjQAr6J [22].}}}An Iranian ne
                                                  //| ws agency thought an Onion article was real--and plagiarized it: http://t.c
                                                  //| o/C4lWe1Ij [112].}Apple
                                                  //| Output exceeds cutoff limit.
allTweets count                                   //> res14: Int = 100
val tweet: Tweet = allTweets mostRetweeted        //> tweet  : objsets.Tweet = iPhone 5's brain dissected. Guess what, it's made 
                                                  //| by Samsung. http://t.co/wSyjvpDc [321]
}