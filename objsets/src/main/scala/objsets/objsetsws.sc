package objsets

object objsetsws {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet

  val ets = new Empty                             //> ets  : objsets.Empty = .
  ets incl new Tweet("ovd", "abcde", 0)           //> res0: objsets.TweetSet = {.abcde [0].}
  ets incl new Tweet("ovd", "zyxw", 0)            //> res1: objsets.TweetSet = {.zyxw [0].}
  ets incl new Tweet("ovd", "abcde", 0) incl new Tweet("ovd", "zyxw", 0)
                                                  //> res2: objsets.TweetSet = {.abcde [0]{.zyxw [0].}}
  val ts1 = ets incl new Tweet("ovd", "abcde", 0) incl new Tweet("ovd", "zyxw", 0) incl new Tweet("ovd", "mnopq", 0)
                                                  //> ts1  : objsets.TweetSet = {.abcde [0]{{.mnopq [0].}zyxw [0].}}
  val ts2 = ets incl new Tweet("ovd", "zyxw", 0) incl new Tweet("ovd", "abcde", 0) incl new Tweet("ovd", "mnopq", 0)
                                                  //> ts2  : objsets.TweetSet = {{.abcde [0]{.mnopq [0].}}zyxw [0].}
  val ts3 = ets incl new Tweet("ovd", "mnopq", 0) incl new Tweet("ovd", "abcde", 0) incl new Tweet("ovd", "zyxw", 0)
                                                  //> ts3  : objsets.TweetSet = {{.abcde [0].}mnopq [0]{.zyxw [0].}}

  ts2                                             //> res3: objsets.TweetSet = {{.abcde [0]{.mnopq [0].}}zyxw [0].}
  ts1 union ts2                                   //> res4: objsets.TweetSet = {{.abcde [0]{.mnopq [0].}}zyxw [0].}
  ts1                                             //> res5: objsets.TweetSet = {.abcde [0]{{.mnopq [0].}zyxw [0].}}
  ts2 union ts1                                   //> res6: objsets.TweetSet = {.abcde [0]{{.mnopq [0].}zyxw [0].}}
  ts1 union ts2 union ts3                         //> res7: objsets.TweetSet = {{.abcde [0].}mnopq [0]{.zyxw [0].}}
  ts1 union ets                                   //> res8: objsets.TweetSet = {.abcde [0]{{.mnopq [0].}zyxw [0].}}
  ets union ets                                   //> res9: objsets.TweetSet = .

  ets filter ((x: Tweet) => x.text > "a")         //> res10: objsets.TweetSet = .
  ets incl new Tweet("ovd", "abcde", 1) filter (x => x.text > "a")
                                                  //> res11: objsets.TweetSet = {.abcde [1].}
  ets incl new Tweet("ovd", "abcde", 2) filter (x => x.text < "zzzzz")
                                                  //> res12: objsets.TweetSet = {.abcde [2].}
  ets incl new Tweet("ovd", "abcde", 3) filterAcc (x => x.retweets > 0, ets)
                                                  //> res13: objsets.TweetSet = {.abcde [3].}
  ets incl new Tweet("ovd", "a", 4) incl new Tweet("ovd", "b", 2) incl new Tweet("ovd", "c", 1)
                                                  //> res14: objsets.TweetSet = {.a [4]{.b [2]{.c [1].}}}
  ets incl new Tweet("ovd", "a", 4) incl new Tweet("ovd", "b", 2) incl new Tweet("ovd", "c", 1) filter (x => x.retweets % 2 == 0)
                                                  //> res15: objsets.TweetSet = {.a [4]{.b [2].}}

  ets incl new Tweet("ovd", "a", 14) incl new Tweet("ovd", "z", 23) incl new Tweet("ovd", "c", 11) incl new Tweet("ovd", "ggh", 41) incl new Tweet("ovd", "ooo", 2) incl new Tweet("ovd", "cww", 21) mostRetweeted
                                                  //> res16: objsets.Tweet = ggh [41]

  ets incl new Tweet("ovd", "a", 0) incl new Tweet("ovd", "b", 0) incl new Tweet("ovd", "c", 0) descendingByRetweet
                                                  //> res17: objsets.TweetList = objsets.Cons@10dd95f6

  ets incl new Tweet("ovd", "a", 0) incl new Tweet("ovd", "b", 0) incl new Tweet("ovd", "c", 0) mostRetweeted
                                                  //> res18: objsets.Tweet = a [0]
  
  (ets incl new Tweet("ovd", "a", 14) incl new Tweet("ovd", "z", 23)) union (ets incl new Tweet("ovd", "c", 11) incl new Tweet("ovd", "hhh", 41) incl new Tweet("ovd", "ooo", 2) incl new Tweet("ovd", "cww", 21))
                                                  //> res19: objsets.TweetSet = {{.a [14].}c [11]{{.cww [21].}hhh [41]{.ooo [2]{.
                                                  //| z [23].}}}}
}