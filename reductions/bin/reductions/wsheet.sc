package reductions

import common._

object wsheet {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet

  def countChange(money: Int, coins: List[Int]): Int = {
    def useCoins(money: Int, coins: List[Int]): Int =
      if (money == 0) 1
      else if (coins.isEmpty) 0
      else useCoin(money, coins.head, money / coins.head, coins.tail)

    def useCoin(money: Int, coin: Int, occurences: Int, otherCoins: List[Int]): Int =
      if (occurences == 0) useCoins(money, otherCoins)
      else if (money - coin * occurences == 0) 1 + useCoin(money, coin, occurences - 1, otherCoins)
      else useCoins(money - coin * occurences, otherCoins) + useCoin(money, coin, occurences - 1, otherCoins)

    if (money < 0) throw new IllegalArgumentException("money")
    else useCoins(money, coins)
  }                                               //> countChange: (money: Int, coins: List[Int])Int

  countChange(250, List(1, 2, 5, 10, 20, 50))     //> res0: Int = 177863

  def countChange2(money: Int, coins: List[Int]): Int = {
    if (money == 0) 1
    else if (money < 0) 0
    else coins match {
      case List() => 0
      case List(coin) => if (money % coin == 0) 1 else 0
      case _ => countChange2(money - coins.head, coins) + countChange2(money, coins.tail)
    }
  }                                               //> countChange2: (money: Int, coins: List[Int])Int

  countChange2(4, List(1, 2))                     //> res1: Int = 3

  type Threshold = (Int, List[Int]) => Boolean

  def parCountChange(money: Int, coins: List[Int], threshold: Threshold): Int = {
    if (money == 0) 1
    else if (money < 0) 0
    else if (threshold(money, coins)) {
			countChange(money, coins)
		}
    else coins match {
      case List() => 0
      case _ => {
      	println("parallel")
        val (a,b) = parallel(parCountChange(money - coins.head, coins, threshold), parCountChange(money, coins.tail, threshold))
        a + b
      }
    }
  }                                               //> parCountChange: (money: Int, coins: List[Int], threshold: reductions.wsheet
                                                  //| .Threshold)Int
  
    def moneyThreshold(startingMoney: Int): Threshold =
    (currentMoney, _) => currentMoney <= 0 || currentMoney <= 2 * startingMoney / 3
                                                  //> moneyThreshold: (startingMoney: Int)reductions.wsheet.Threshold
  
  
  parCountChange(16, List(1), moneyThreshold(16)) //> parallel
                                                  //| parallel
                                                  //| parallel
                                                  //| parallel
                                                  //| parallel
                                                  //| parallel
                                                  //| res2: Int = 1
                                                  
  val cm = 10                                     //> cm  : Int = 10
  val sm = 16                                     //> sm  : Int = 16
  cm <= 2 * sm / 3                                //> res3: Boolean = true
}