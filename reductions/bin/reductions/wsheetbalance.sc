package reductions

import common._

object wsheetbalance {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  def balance(chars: Array[Char]): Boolean = {
    def bal(chars: Array[Char], from: Int, until: Int, p: Int): Boolean = {
      if (from >= until) p == 0
      else if (p < 0) false
      else if (chars(from) == '(') bal(chars, from+1, until, p + 1)
      else if (chars(from) == ')') bal(chars, from+1, until, p - 1)
      else bal(chars, from+1, until, p)
    }
    bal(chars, 0, chars.length, 0)
  }                                               //> balance: (chars: Array[Char])Boolean
  
  def parBalance(chars: Array[Char], threshold: Int): Boolean = {

    def traverse(idx: Int, until: Int, orphanLeft: Int, orphanRight: Int): (Int,Int) = {
    	//println(s"traverse($idx,$until,${(orphanLeft,orphanRight)}")
      if (idx >= until) (orphanLeft,orphanRight)
      else if (chars(idx) == '(') { //println(s"left,$idx,$until,${(orphanLeft,orphanRight)}")
      	traverse(idx+1, until, orphanLeft+1, orphanRight)
      }
      else if (chars(idx) == ')') { //println(s"right,$idx,$until,${(orphanLeft,orphanRight)}")
      	if (orphanLeft > 0) traverse(idx+1, until, orphanLeft-1, orphanRight)
      	else traverse(idx+1, until, 0, orphanRight+1)
      }
      else traverse(idx+1, until, orphanLeft, orphanRight)
    }

    def reduce(from: Int, until: Int): (Int,Int) = {
      if (until-from <= threshold) {
        val (a,b) = traverse(from, until, 0, 0)
        //println(s"$from,$until,${(a,b)}")
        (a,b)
      }
      else {
        val mid = from + (until-from)/2
        f(parallel(reduce(from,mid), reduce(mid,until)))
      }
    }
    
    def f(x:((Int,Int),(Int,Int))):(Int,Int) = x match {
    	case ((l0,r0),(l1,r1)) => {
    		val temp = ((if (l0 > r1) l0-r1 else 0, r0), (l1, if (r1 > l0) r1-l0 else 0))
    		(temp._1._1+temp._2._1, temp._1._2+temp._2._2)
    	}
    }

    reduce(0, chars.length) == (0,0)
  }                                               //> parBalance: (chars: Array[Char], threshold: Int)Boolean
  
  
  balance("(if (zero? x) max (/ 1 x))".toArray)   //> res0: Boolean = true
  balance("I told him (that it's not (yet) done). (But he wasn't listening)".toArray)
                                                  //> res1: Boolean = true
  balance("(o_()".toArray)                        //> res2: Boolean = false
  balance(":-)".toArray)                          //> res3: Boolean = false
  balance("())(".toArray)                         //> res4: Boolean = false
  balance(")(".toArray)                           //> res5: Boolean = false

  parBalance("(if (zero? x) max (/ 1 x))".toArray, 3)
                                                  //> res6: Boolean = true
  parBalance("(()())".toArray, 3)                 //> res7: Boolean = true

  parBalance("I told him (that it's not (yet) done). (But he wasn't listening)".toArray, 3)
                                                  //> res8: Boolean = true
  parBalance("(o_()".toArray, 3)                  //> res9: Boolean = false
  parBalance(":-)".toArray, 3)                    //> res10: Boolean = false
  parBalance("())(".toArray, 3)                   //> res11: Boolean = false
  parBalance(")(".toArray, 1)                     //> res12: Boolean = false
}