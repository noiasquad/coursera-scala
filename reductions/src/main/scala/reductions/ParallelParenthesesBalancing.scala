package reductions

import scala.annotation._
import org.scalameter._
import common._

object ParallelParenthesesBalancingRunner {

  @volatile var seqResult = false

  @volatile var parResult = false

  val standardConfig = config(
    Key.exec.minWarmupRuns -> 40,
    Key.exec.maxWarmupRuns -> 80,
    Key.exec.benchRuns -> 120,
    Key.verbose -> true
  ) withWarmer(new Warmer.Default)

  def main(args: Array[String]): Unit = {
    val length = 100000000
    val chars = new Array[Char](length)
    val threshold = 10000
    val seqtime = standardConfig measure {
      seqResult = ParallelParenthesesBalancing.balance(chars)
    }
    println(s"sequential result = $seqResult")
    println(s"sequential balancing time: $seqtime ms")

    val fjtime = standardConfig measure {
      parResult = ParallelParenthesesBalancing.parBalance(chars, threshold)
    }
    println(s"parallel result = $parResult")
    println(s"parallel balancing time: $fjtime ms")
    println(s"speedup: ${seqtime / fjtime}")
  }
}

object ParallelParenthesesBalancing {

  /** Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def balance(chars: Array[Char]): Boolean = {
    def bal(chars: Array[Char], from: Int, to: Int, p: Int): Boolean =
      if (to <= from) p == 0
      else if (p < 0) false
      else if (chars(from) == '(') bal(chars, from+1, to, p + 1)
      else if (chars(from) == ')') bal(chars, from+1, to, p - 1)
      else bal(chars, from+1, to, p)
    bal(chars, 0, chars.length, 0)
  }
 
  /** Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def parBalance(chars: Array[Char], threshold: Int): Boolean = {
    
    def traverse(idx: Int, until: Int, orphanLeft: Int, orphanRight: Int): (Int,Int) = {
      if (idx >= until) (orphanLeft,orphanRight)
      else if (chars(idx) == '(')
        traverse(idx+1, until, orphanLeft+1, orphanRight)
      else if (chars(idx) == ')') {
        if (orphanLeft > 0) traverse(idx+1, until, orphanLeft-1, orphanRight)
        else traverse(idx+1, until, 0, orphanRight+1)
      }
      else traverse(idx+1, until, orphanLeft, orphanRight)
    }

    def reduce(from: Int, until: Int): (Int,Int) = {
      if (until-from <= threshold) {
        traverse(from, until, 0, 0)
      }
      else {
        val mid = from + (until-from)/2
        f(parallel(reduce(from,mid), reduce(mid,until)))
      }
    }
    
    def f(x:((Int,Int),(Int,Int))):(Int,Int) = x match {
      case ((l0,r0),(l1,r1)) => {
        val temp = ((if (l0 > r1) l0-r1 else 0, r0), (l1, if (r1 > l0) r1-l0 else 0))
        (temp._1._1+temp._2._1, temp._1._2+temp._2._2)
      }
    }

    reduce(0, chars.length) == (0,0)
  }

  // For those who want more:
  // Prove that your reduction operator is associative!

}
