package reductions

import common._

object wsheetlineofsight {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  def max(a: Float, b: Float): Float = if (a > b) a else b
                                                  //> max: (a: Float, b: Float)Float
  
  def lineOfSight(input: Array[Float], output: Array[Float]): Unit = {
    var maxAngle = 0.0f
    output(0) = 0.0f
    var idx = 1
    while (idx < input.length) {
      maxAngle = max(input(idx) / idx, maxAngle)
      output(idx) = maxAngle
      idx = idx+1
    }
  }                                               //> lineOfSight: (input: Array[Float], output: Array[Float])Unit
  
  val length = 50                                 //> length  : Int = 50
  val input = (0 until length).map(_ % 9 * 1.0f).toArray
                                                  //> input  : Array[Float] = Array(0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 0
                                                  //| .0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.
                                                  //| 0, 7.0, 8.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 0.0, 1.0, 2.0, 3.0
                                                  //| , 4.0, 5.0, 6.0, 7.0, 8.0, 0.0, 1.0, 2.0, 3.0, 4.0)
  val output = new Array[Float](length + 1)       //> output  : Array[Float] = Array(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                                  //| 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0
                                                  //| .0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.
                                                  //| 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
  lineOfSight(input, output)
  output                                          //> res0: Array[Float] = Array(0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
                                                  //|  1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 
                                                  //| 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1
                                                  //| .0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0)
  
  sealed abstract class Tree {
    def maxPrevious: Float
  }

  case class Node(left: Tree, right: Tree) extends Tree {
    val maxPrevious = max(left.maxPrevious, right.maxPrevious)
  }

  case class Leaf(from: Int, until: Int, maxPrevious: Float) extends Tree

  def upsweepSequential(input: Array[Float], from: Int, until: Int): Float = {
    var maxAngle = Float.MinValue
    var idx = from
    while (idx < until) {
      maxAngle = max(input(idx) / idx, maxAngle)
      idx = idx+1
    }
    maxAngle
  }                                               //> upsweepSequential: (input: Array[Float], from: Int, until: Int)Float

	def upsweep(input: Array[Float], from: Int, end: Int, threshold: Int): Tree = {
		println(s"upsweep($from,$end)")
    if (end-from <= threshold) {
      Leaf(from, end, upsweepSequential(input, from, end))
    }
    else {
      val mid = from + (end-from)/2
      println(s"parallel upsweep($from,$mid,$mid,$end)")
      val (treeL, treeR) = parallel(upsweep(input, from, mid, threshold), upsweep(input, mid, end, threshold))
      Node(treeL, treeR)
    }
  }                                               //> upsweep: (input: Array[Float], from: Int, end: Int, threshold: Int)reductio
                                                  //| ns.wsheetlineofsight.Tree

  def downsweepSequential(input: Array[Float], output: Array[Float], startingAngle: Float, from: Int, until: Int): Unit = {
    var maxAngle = startingAngle
    var idx = 0
    while (idx < input.length) {
      maxAngle = max(input(idx) / idx, maxAngle)
      output(idx) = maxAngle
      idx = idx+1
    }
  }                                               //> downsweepSequential: (input: Array[Float], output: Array[Float], startingAn
                                                  //| gle: Float, from: Int, until: Int)Unit
  def downsweep(input: Array[Float], output: Array[Float], startingAngle: Float, tree: Tree): Unit = tree match {
    case Leaf(from, end, maxPrevious) => downsweepSequential(input, output, startingAngle, from, end)
    case Node(left,right) => {
    	println("parallel downsweep")
      parallel(
        downsweep(input, output, startingAngle, left),
        downsweep(input, output, max(startingAngle,left.maxPrevious), right))
    }
  }                                               //> downsweep: (input: Array[Float], output: Array[Float], startingAngle: Float
                                                  //| , tree: reductions.wsheetlineofsight.Tree)Unit
  def parLineOfSight(input: Array[Float], output: Array[Float], threshold: Int): Unit = {
    val tree = upsweep(input, 1, input.length, threshold)
    downsweep(input, output, 0f, tree)
    output(0) = 0f
  }                                               //> parLineOfSight: (input: Array[Float], output: Array[Float], threshold: Int)
                                                  //| Unit

	val inp = Array[Float](0f, 1f, 8f, 9f, 7f, 1f, 8f, 9f, 7f, 1f, 8f, 9f, 7f, 1f, 8f, 9f, 7f)
                                                  //> inp  : Array[Float] = Array(0.0, 1.0, 8.0, 9.0, 7.0, 1.0, 8.0, 9.0, 7.0, 1.
                                                  //| 0, 8.0, 9.0, 7.0, 1.0, 8.0, 9.0, 7.0)
	val out = Array[Float](0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f)
                                                  //> out  : Array[Float] = Array(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.
                                                  //| 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)


	//upsweep(inp, 1, inp.length, 1)
	
	parLineOfSight(inp, out, 4)               //> upsweep(1,17)
                                                  //| parallel upsweep(1,9,9,17)
                                                  //| upsweep(1,9)
                                                  //| upsweep(9,17)
                                                  //| parallel upsweep(1,5,5,9)
                                                  //| parallel upsweep(9,13,13,17)
                                                  //| upsweep(1,5)
                                                  //| upsweep(9,13)
                                                  //| upsweep(13,17)
                                                  //| upsweep(5,9)
                                                  //| parallel downsweep
                                                  //| parallel downsweep
                                                  //| parallel downsweep
  out                                             //> res1: Array[Float] = Array(0.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0
                                                  //| , 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0)
}