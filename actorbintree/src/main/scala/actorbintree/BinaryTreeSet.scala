/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import akka.actor._
import scala.collection.immutable.Queue


object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef
    def id: Int
    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier `id` to insert an element `elem` into the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to check whether an element `elem` is present
    * in the tree. The actor at reference `requester` should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to remove the element `elem` from the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection*/
  case object GC

  /** Holds the answer to the Contains request with identifier `id`.
    * `result` is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply
  
  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}


class BinaryTreeSet extends Actor with akka.actor.ActorLogging {
  import BinaryTreeSet._
  import BinaryTreeNode._
  
  def createRoot: ActorRef = context.actorOf(props(0, initiallyRemoved = true))

  var root = createRoot

  // optional
  var pendingQueue = Queue.empty[Operation]

  // optional
  def receive = normal

  // optional
  /** Accepts `Operation` and `GC` messages. */
  val normal: Receive = {
    case op: Operation => log.debug("{}", op); root ! op;

    case GC => {
      if (pendingQueue.isEmpty) {
        log.info("garbage collecting ordered, switch to garbage collecting behavior"); 
        val newRoot = context.actorOf(props(0, true))
        root ! CopyTo(newRoot)
        context.become(garbageCollecting(newRoot))
      }
    }
  }

  // optional
  /** Handles messages while garbage collection is performed.
    * `newRoot` is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef): Receive = {
    case op: Operation => log.debug("enqueue {}", op); pendingQueue = pendingQueue.enqueue(op)

    case CopyFinished => {
      root ! PoisonPill
      
      log.info("garbage collecting, replay enqueued operations ({})", pendingQueue.size); 
      pendingQueue.map(op => {log.info("replay {}", op); newRoot ! op})
      pendingQueue = Queue.empty
      
      root = newRoot

      log.info("end of garbage collecting, all enqueued operations have been replayed, back to normal behavior");
      context become normal
    }
    
    case GC =>
  }

}

object BinaryTreeNode {
  trait Position

  case object Left extends Position
  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)
  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode], elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor with akka.actor.ActorLogging {
  import BinaryTreeNode._
  import BinaryTreeSet._

  var subtrees = Map[Position, ActorRef]()
  var removed = initiallyRemoved

  // optional
  def receive = normal

  // optional
  /** Handles `Operation` messages and `CopyTo` requests. */
  val normal: Receive = { 
    case op@Insert(issuer, seq, e) =>
       if (e < elem) {
        if (subtrees.contains(Left))
          subtrees(Left) ! Insert(issuer, seq, e)
        else {
          subtrees = subtrees + ((Left, context.actorOf(props(e, false), e.toString())))
          log.debug("operation finished {}", op);
          issuer ! OperationFinished(seq)
        }
      }
      else if (e > elem) {
        if (subtrees.contains(Right))
          subtrees(Right) ! Insert(issuer, seq, e)
        else {
          subtrees = subtrees + ((Right, context.actorOf(props(e, false), e.toString())))
          log.debug("operation finished {}", op);
          issuer ! OperationFinished(seq)
        }
      }
      else if (removed) {
        if (subtrees.contains(Left))
          subtrees(Left) ! Insert(issuer, seq, e)
        else {
          subtrees = subtrees + ((Left, context.actorOf(props(e, false), e.toString())))
          log.debug("operation finished {}", op);
          issuer ! OperationFinished(seq)
        }
      }
      else {
        removed = false
        log.debug("operation finished {}", op);
        issuer ! OperationFinished(seq)
      }
          
    case op@Remove(issuer, seq, e) =>
      if (e < elem) {
        if (subtrees.contains(Left))
          subtrees(Left) ! Remove(issuer, seq, e)
        else {
          log.debug("operation finished {}", op);
          issuer ! OperationFinished(seq)
        }
      }
      else if (e > elem) {
        if (subtrees.contains(Right))
          subtrees(Right) ! Remove(issuer, seq, e)
        else {
          log.debug("operation finished {}", op);
          issuer ! OperationFinished(seq)
        }
      }
      else if (removed) {
        if (subtrees.contains(Left))
          subtrees(Left) ! Remove(issuer, seq, e)
        else {
          log.debug("operation finished {}", op);
          issuer ! OperationFinished(seq)
        }
      }
      else {
          removed = true
          log.debug("operation finished {}", op);
          issuer ! OperationFinished(seq)
      }
      
    case op@Contains(issuer, seq, e) =>
      if (e < elem) {
        if (subtrees.contains(Left))
          subtrees(Left) ! Contains(issuer, seq, e)
        else {
          log.debug("contains result {}:{}", op,false);
          issuer ! ContainsResult(seq, false)
        }
      }
      else if (e > elem) {
        if (subtrees.contains(Right))
          subtrees(Right) ! Contains(issuer, seq, e)
        else {
          log.debug("contains result {}:{}", op,false);
          issuer ! ContainsResult(seq, false)
        }
      }
      else if (removed) {
        if (subtrees.contains(Left))
          subtrees(Left) ! Contains(issuer, seq, e)
        else {
          log.debug("contains result {}:{}", op,false);
          issuer ! ContainsResult(seq, false)
        }
      }
      else {
          log.debug("contains result {}:{}", op, true);
          issuer ! ContainsResult(seq, true)
      }
      
    case CopyTo(newRoot: ActorRef) => {
      if (!removed)
        newRoot ! Insert(self, 0, elem)
        
      if (removed && subtrees.isEmpty)
        sender ! CopyFinished
      else {
        log.debug("has elem {} and switch to copying behavior with refs {}", elem, subtrees.values.toSet);
        context become copying(subtrees.values.toSet, insertConfirmed = removed, sender)
      }
      
      subtrees.values foreach(_ ! CopyTo(newRoot))
    }
  }

  // optional
  /** `expected` is the set of ActorRefs whose replies we are waiting for,
    * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean, issuer: ActorRef): Receive = {
    case OperationFinished(_) => {
      if (expected.isEmpty) {
        log.debug("has elem {}, has been copied, every children copied, notify issuer", elem);        
        issuer ! CopyFinished
        context become normal
      }
      else {
        log.debug("has elem {}, has been copied, but still expecting children, stay copying", elem);
        context become copying(expected, true, issuer)
      }
    }
    case CopyFinished => {
      val newExpected = expected-sender
      if (newExpected.isEmpty && insertConfirmed) {
          log.debug("has elem {}, last child copied, has already been copied, notify issuer", elem); 
          issuer ! CopyFinished
          context become normal
        }
        else {
          log.debug("has elem {}, last child copied, has yet not been copied, waiting", elem);
          context become copying(newExpected, insertConfirmed, issuer)
        }
    }
  }

}
