package streams

object bloxorzws {

  case class Pos(val x: Int, val y: Int) {
    override def toString = "(" + x + "," + y + ")"
  }

  val level =
    """ooo-------
        |oSoooo----
        |ooooooooo-
        |-ooooooooo
        |-----ooToo
        |------ooo-""".stripMargin                //> level  : String = ooo-------
                                                  //| oSoooo----
                                                  //| ooooooooo-
                                                  //| -ooooooooo
                                                  //| -----ooToo
                                                  //| ------ooo-
  val levelVector = Vector(level.split("\n").map(str => Vector(str: _*)): _*)
                                                  //> levelVector  : scala.collection.immutable.Vector[scala.collection.immutable.
                                                  //| Vector[Char]] = Vector(Vector(o, o, o, -, -, -, -, -, -, -), Vector(o, S, o,
                                                  //|  o, o, o, -, -, -, -), Vector(o, o, o, o, o, o, o, o, o, -), Vector(-, o, o,
                                                  //|  o, o, o, o, o, o, o), Vector(-, -, -, -, -, o, o, T, o, o), Vector(-, -, -,
                                                  //|  -, -, -, o, o, o, -))

  levelVector.size                                //> res0: Int = 6

  levelVector(5)                                  //> res1: scala.collection.immutable.Vector[Char] = Vector(-, -, -, -, -, -, o, 
                                                  //| o, o, -)
  levelVector(5).indexWhere((c: Char) => c == '-')//> res2: Int = 0
  levelVector(5).indexWhere((c: Char) => c == 'o')//> res3: Int = 6

  def terrainFunction(levelVector: Vector[Vector[Char]]): Pos => Boolean =
    (pos: Pos) =>
      pos.x >= 0 &&
        pos.y >= 0 &&
        pos.x < levelVector.size &&
        pos.y < levelVector(pos.x).size &&
        levelVector(pos.x)(pos.y) != '-'          //> terrainFunction: (levelVector: Vector[Vector[Char]])streams.bloxorzws.Pos =>
                                                  //|  Boolean

  terrainFunction(levelVector)(Pos(5, 6))         //> res4: Boolean = true

  for {
    x <- 0 to 10
    y <- 0 to 10
    if (terrainFunction(levelVector)(Pos(x, y)))
  } yield (x, y)                                  //> res5: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((0,0), (0,1
                                                  //| ), (0,2), (1,0), (1,1), (1,2), (1,3), (1,4), (1,5), (2,0), (2,1), (2,2), (2,
                                                  //| 3), (2,4), (2,5), (2,6), (2,7), (2,8), (3,1), (3,2), (3,3), (3,4), (3,5), (3
                                                  //| ,6), (3,7), (3,8), (3,9), (4,5), (4,6), (4,7), (4,8), (4,9), (5,6), (5,7), (
                                                  //| 5,8))

  def findChar(char: Char, levelVector: Vector[Vector[Char]]): Pos = {
    def scan(line: Int): Pos = {
      val column = (levelVector(line).indexWhere(c => c == char))
      if (column < 0) scan(line + 1)
      else Pos(line, column)
    }
    scan(0)
  }                                               //> findChar: (char: Char, levelVector: Vector[Vector[Char]])streams.bloxorzws.
                                                  //| Pos
  findChar('S', levelVector)                      //> res6: streams.bloxorzws.Pos = (1,1)
  findChar('T', levelVector)                      //> res7: streams.bloxorzws.Pos = (4,7)

  case class Block(b1: Pos, b2: Pos) {
    def isStanding: Boolean = b1 == b2
  }
  
  Block(Pos(1,1),Pos(2,2)).isStanding             //> res8: Boolean = false
  Block(Pos(1,1),Pos(1,2)).isStanding             //> res9: Boolean = false
  Block(Pos(1,1),Pos(2,1)).isStanding             //> res10: Boolean = false
  Block(Pos(1,1),Pos(1,1)).isStanding             //> res11: Boolean = true
  
  val st = Stream(1,2,3)                          //> st  : scala.collection.immutable.Stream[Int] = Stream(1, ?)
  val st2 = Stream(9,8,7)                         //> st2  : scala.collection.immutable.Stream[Int] = Stream(9, ?)
  val se = Set(8,5,4)                             //> se  : scala.collection.immutable.Set[Int] = Set(8, 5, 4)
  val li = List(8,5,4)                            //> li  : List[Int] = List(8, 5, 4)
  
  st.tail ++ se                                   //> res12: scala.collection.immutable.Stream[Int] = Stream(2, ?)
  st ++ st2                                       //> res13: scala.collection.immutable.Stream[Int] = Stream(1, ?)
  
  se ++ st                                        //> res14: scala.collection.immutable.Set[Int] = Set(5, 1, 2, 3, 8, 4)
  li ++ st                                        //> res15: List[Int] = List(8, 5, 4, 1, 2, 3)
  
  11 #:: st                                       //> res16: scala.collection.immutable.Stream[Int] = Stream(11, ?)
}