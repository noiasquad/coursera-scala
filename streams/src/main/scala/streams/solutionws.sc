package streams

object solutionws {

  trait Level1 extends GameDef with Solver with StringParserTerrain {
    val level =
      """ooo-------
      |oSoooo----
      |ooooooooo-
      |-ooooooooo
      |-----ooToo
      |------ooo-""".stripMargin
  }
	object level1 extends Level1
  level1.solution                                 //> res0: List[streams.solutionws.level1.Move] = List(Right, Right, Down, Right,
                                                  //|  Right, Right, Down)

  trait Level2 extends GameDef with Solver with StringParserTerrain {
    val level =
      """ooo-------
      |oooooS----
      |ooooooooo-
      |-ooooooooo
      |-----Toooo
      |------ooo-""".stripMargin
  }
	object level2 extends Level2
  level2.solution                                 //> res1: List[streams.solutionws.level2.Move] = List(Left, Left, Down, Right, R
                                                  //| ight, Right, Down)
}