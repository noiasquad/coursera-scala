package calculator

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double],
      c: Signal[Double]): Signal[Double] = {
    Signal {
      val va = a()
      val vb = b()
      val vc = c()
      vb*vb - 4*va*vc
    }
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double],
      c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
    Signal {
      val va = a()
      val vb = b()
      val vc = c()
      val vdelta = delta()
      if (vdelta < 0.0) Set()
      else if (vdelta == 0.0) Set(-vb/(2.0*va))
      else {
        val square = Math.sqrt(vdelta)
        Set((-vb + square)/(2.0*va), (-vb - square)/(2.0*va))  
      }
    }
  }
}
