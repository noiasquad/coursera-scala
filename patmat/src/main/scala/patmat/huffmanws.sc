package patmat

object huffmanws {
  import Huffman._

  string2Chars("aa")                              //> res0: List[Char] = List(a, a)
  times(List('b', 'a', 'a'))                      //> res1: List[(Char, Int)] = List((b,1), (a,2))
  timesOrdered(string2Chars("abbccc"))            //> res2: List[(Char, Int)] = List((a,1), (b,2), (c,3))

  val codeTreeLeaves = makeOrderedLeafList(times(string2Chars("abbccc")))
                                                  //> codeTreeLeaves  : List[patmat.Huffman.Leaf] = List(Leaf(a,1), Leaf(b,2), Lea
                                                  //| f(c,3))
                                                  
                                                  makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3)))
                                                  //> res3: List[patmat.Huffman.Leaf] = List(Leaf(e,1), Leaf(t,2), Leaf(x,3))

    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
                                                  //> leaflist  : List[patmat.Huffman.Leaf] = List(Leaf(e,1), Leaf(t,2), Leaf(x,4)
                                                  //| )
    combine(leaflist)                             //> res4: List[patmat.Huffman.CodeTree] = List(Fork(Leaf(e,1),Leaf(t,2),List(e, 
                                                  //| t),3), Leaf(x,4))


  singleton(codeTreeLeaves)                       //> res5: Boolean = false

  isort(string2Chars("uzoiurewahlfasdhf"), (c1: Char, c2: Char) => c1 <= c2)
                                                  //> res6: List[Char] = List(a, a, d, e, f, f, h, h, i, l, o, r, s, u, u, w, z)

  val codeTreeSingleton = combine(codeTreeLeaves) //> codeTreeSingleton  : List[patmat.Huffman.CodeTree] = List(Fork(Leaf(a,1),Lea
                                                  //| f(b,2),List(a, b),3), Leaf(c,3))
  singleton(codeTreeSingleton)                    //> res7: Boolean = false

  val codeTree = createCodeTree(string2Chars("abbccc"))
                                                  //> codeTree  : patmat.Huffman.CodeTree = Fork(Fork(Leaf(a,1),Leaf(b,2),List(a, 
                                                  //| b),3),Leaf(c,3),List(a, b, c),6)

  encode(codeTree)(string2Chars("abbcccc"))       //> res8: List[patmat.Huffman.Bit] = List(0, 0, 0, 1, 0, 1, 1, 1, 1, 1)
  
  slowCodeBits(codeTree)('a')                     //> res9: List[patmat.Huffman.Bit] = List(0, 0)

  val ct = createCodeTree(string2Chars("abbcccc"))//> ct  : patmat.Huffman.CodeTree = Fork(Fork(Leaf(a,1),Leaf(b,2),List(a, b),3),
                                                  //| Leaf(c,4),List(a, b, c),7)
  slowCodeBits(ct)('a')                           //> res10: List[patmat.Huffman.Bit] = List(0, 0)
  slowCodeBits(ct)('b')                           //> res11: List[patmat.Huffman.Bit] = List(0, 1)
  slowCodeBits(ct)('c')                           //> res12: List[patmat.Huffman.Bit] = List(1)

	convert(ct)                               //> res13: patmat.Huffman.CodeTable = List((a,List(0, 0)), (b,List(0, 1)), (c,Li
                                                  //| st(1)))

  encode(ct)(string2Chars("ab"))                  //> res14: List[patmat.Huffman.Bit] = List(0, 0, 0, 1)
  encode(ct)(string2Chars("aaab"))                //> res15: List[patmat.Huffman.Bit] = List(0, 0, 0, 0, 0, 0, 0, 1)
  encode(ct)(string2Chars("ccc"))                 //> res16: List[patmat.Huffman.Bit] = List(1, 1, 1)
  
  quickEncode(ct)(string2Chars("ab"))             //> res17: List[patmat.Huffman.Bit] = List(0, 0, 0, 1)
  quickEncode(ct)(string2Chars("aaab"))           //> res18: List[patmat.Huffman.Bit] = List(0, 0, 0, 0, 0, 0, 0, 1)
  quickEncode(ct)(string2Chars("ccc"))            //> res19: List[patmat.Huffman.Bit] = List(1, 1, 1)
  quickEncode(ct)(string2Chars("a"))              //> res20: List[patmat.Huffman.Bit] = List(0, 0)
  quickEncode(ct)(string2Chars("b"))              //> res21: List[patmat.Huffman.Bit] = List(0, 1)
  quickEncode(ct)(string2Chars("c"))              //> res22: List[patmat.Huffman.Bit] = List(1)
 
 	val lct = createCodeTree(string2Chars("a"))
                                                  //> lct  : patmat.Huffman.CodeTree = Leaf(a,1)
	decode(lct, quickEncode(lct)(List()))     //> res23: List[Char] = List()
	decode(ct, quickEncode(ct)(string2Chars("a")))
                                                  //> res24: List[Char] = List(a)
	decode(ct, quickEncode(ct)(string2Chars("b")))
                                                  //> res25: List[Char] = List(b)
	decode(ct, quickEncode(ct)(string2Chars("c")))
                                                  //> res26: List[Char] = List(c)
	decode(ct, quickEncode(ct)(string2Chars("ab")))
                                                  //> res27: List[Char] = List(a, b)
	decode(ct, quickEncode(ct)(string2Chars("aaab")))
                                                  //> res28: List[Char] = List(a, a, a, b)
	decode(ct, quickEncode(ct)(string2Chars("ccaaabbbabc")))
                                                  //> res29: List[Char] = List(c, c, a, a, a, b, b, b, a, b, c)
  
  val frenchCodeTable = convert(frenchCode)       //> frenchCodeTable  : patmat.Huffman.CodeTable = List((s,List(0, 0, 0)), (d,Li
                                                  //| st(0, 0, 1, 0)), (x,List(0, 0, 1, 1, 0, 0, 0)), (j,List(0, 0, 1, 1, 0, 0, 1
                                                  //| )), (f,List(0, 0, 1, 1, 0, 1)), (z,List(0, 0, 1, 1, 1, 0, 0, 0, 0)), (k,Lis
                                                  //| t(0, 0, 1, 1, 1, 0, 0, 0, 1, 0)), (w,List(0, 0, 1, 1, 1, 0, 0, 0, 1, 1)), (
                                                  //| y,List(0, 0, 1, 1, 1, 0, 0, 1)), (h,List(0, 0, 1, 1, 1, 0, 1)), (q,List(0, 
                                                  //| 0, 1, 1, 1, 1)), (o,List(0, 1, 0, 0)), (l,List(0, 1, 0, 1)), (m,List(0, 1, 
                                                  //| 1, 0, 0)), (p,List(0, 1, 1, 0, 1)), (u,List(0, 1, 1, 1)), (r,List(1, 0, 0, 
                                                  //| 0)), (c,List(1, 0, 0, 1, 0)), (v,List(1, 0, 0, 1, 1, 0)), (g,List(1, 0, 0, 
                                                  //| 1, 1, 1, 0)), (b,List(1, 0, 0, 1, 1, 1, 1)), (n,List(1, 0, 1, 0)), (t,List(
                                                  //| 1, 0, 1, 1)), (e,List(1, 1, 0)), (i,List(1, 1, 1, 0)), (a,List(1, 1, 1, 1))
                                                  //| )
                                                  
  val encodedText = quickEncode(frenchCode)(string2Chars("ceci est un texte"))
                                                  //> encodedText  : List[patmat.Huffman.Bit] = List(1, 0, 0, 1, 0, 1, 1, 0, 1, 0
                                                  //| , 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0
                                                  //| , 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0)
                                                  
  decode(frenchCode,encodedText)                  //> res30: List[Char] = List(c, e, c, i, e, s, t, u, n, t, e, x, t, e)
  decode(frenchCode, secret)                      //> res31: List[Char] = List(h, u, f, f, m, a, n, e, s, t, c, o, o, l)
  
}